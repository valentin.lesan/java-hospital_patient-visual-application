import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Gestionare_Pacienti extends JFrame{

	private JTable table;
	private ConexiuneBD conn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gestionare_Pacienti window = new Gestionare_Pacienti();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gestionare_Pacienti() {
		setTitle("Pacienti");
		
		try { // initializam conexiunea la DB
			conn = new ConexiuneBD();
			conn.conectarebd();
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Eroare de conexiune : " + exc, "Eroare", JOptionPane.ERROR_MESSAGE);
		}

		setResizable(false);
		setBounds(100, 100, 1067, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(102, 204, 204));
		panel.setBounds(0, 0, 479, 572);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 255, 255));
		panel_2.setBounds(131, 32, 215, 112);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 0, 51));
		panel_3.setBounds(97, 25, 10, 57);
		panel_2.add(panel_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(255, 0, 51));
		panel_4.setBounds(67, 51, 69, 10);
		panel_2.add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(new Color(255, 255, 255));
		panel_5.setBounds(99, 407, 270, 4);
		panel.add(panel_5);
		
		JPanel panel_5_1 = new JPanel();
		panel_5_1.setBackground(Color.WHITE);
		panel_5_1.setBounds(164, 440, 132, 4);
		panel.add(panel_5_1);
		
		JLabel lblNewLabel = new JLabel("Avem grija de sanatatea dvs!");
		lblNewLabel.setFont(new Font("Century Gothic", Font.PLAIN, 28));
		lblNewLabel.setBounds(35, 248, 411, 112);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 51, 102));
		panel_1.setBounds(477, 0, 576, 572);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 555, 187);
		panel_1.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		
//		Block
		ArrayList<Pacient> ar = null;
		ar = conn.afiseazaPacientii();
		ModelTabelPacient model = new ModelTabelPacient(ar);
		
		
		JButton btnNewButton = new JButton("Afisare date");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				table.setModel(model);
			}
		});
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(102, 0, 102));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(25, 280, 143, 33);
		panel_1.add(btnNewButton);
		
		JButton btnDiagnoza = new JButton("Diagnoza");
		btnDiagnoza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					ArrayList<Pacient> ar = null;
				
				String diagnosis = JOptionPane.showInputDialog("Dati diagnoza: (Ex: Gripa)");
				ar = conn.Diagnoza(diagnosis);
				ModelTabelPacient model = new ModelTabelPacient(ar);
				table.setModel(model);
				} catch (NullPointerException ex) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus date!");
				}
				
			}
		});
		btnDiagnoza.setForeground(Color.WHITE);
		btnDiagnoza.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnDiagnoza.setBackground(new Color(102, 0, 102));
		btnDiagnoza.setBounds(203, 280, 143, 33);
		panel_1.add(btnDiagnoza);
		
		JButton btnSectie = new JButton("Sectie");
		btnSectie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String sectia = JOptionPane.showInputDialog("Dati sectia: (Ex: Pneumologie)");
				ArrayList<Pacient> ar = null;
				ar = conn.Pacient_Sectie(sectia);
				ModelTabelPacient model2 = new ModelTabelPacient(ar);
				table.setModel(model2);
				} catch (NullPointerException ie) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus date");
				} catch (NumberFormatException exx2) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus valori / valori incorecte");
				}
			}
		});
		btnSectie.setToolTipText("Lista pacientilor cu sectia introdusa");
		btnSectie.setForeground(Color.WHITE);
		btnSectie.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSectie.setBackground(new Color(102, 0, 102));
		btnSectie.setBounds(25, 429, 143, 33);
		panel_1.add(btnSectie);
		
		JButton btnPacientiDupaPrenume = new JButton("Pacienti dupa prenume");
		btnPacientiDupaPrenume.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String prenume = JOptionPane.showInputDialog("Dati Prenumele Pacientului (Ex: Eugen)");
					ArrayList<Pacient> ar = null;
					ar = conn.Pacient_Prenume(prenume);
					ModelTabelPacient model2 = new ModelTabelPacient(ar);
					table.setModel(model2);
					} catch (NullPointerException ie) {
						JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus date");
					} catch (NumberFormatException exx2) {
						JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus valori / valori incorecte");
					}
			}
		});
		btnPacientiDupaPrenume.setToolTipText("Lista pacientilor dupa prenume");
		btnPacientiDupaPrenume.setForeground(Color.WHITE);
		btnPacientiDupaPrenume.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnPacientiDupaPrenume.setBackground(new Color(102, 0, 102));
		btnPacientiDupaPrenume.setBounds(203, 429, 143, 33);
		panel_1.add(btnPacientiDupaPrenume);
		
		JButton btnAsigurareInterva = new JButton("Asigurare Interval");
		btnAsigurareInterva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String min = JOptionPane.showInputDialog("Dati numarul minim al asigurarii");
				String max = JOptionPane.showInputDialog("Dati numarul maxim al asigurarii");
				ArrayList<Pacient> ar = null;
				ar = conn.Polita_Asigurare_Diapazon(min, max);
				ModelTabelPacient model = new ModelTabelPacient(ar);
				table.setModel(model);
				} catch (NullPointerException exx) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus valori / valori incorecte");
				} catch (NumberFormatException exx2) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus valori / valori incorecte");
				}

			}
		});
		btnAsigurareInterva.setToolTipText("Asigurare Interval");
		btnAsigurareInterva.setForeground(Color.WHITE);
		btnAsigurareInterva.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAsigurareInterva.setBackground(new Color(102, 0, 102));
		btnAsigurareInterva.setBounds(386, 280, 143, 33);
		panel_1.add(btnAsigurareInterva);
		
		JButton btnNumarAsigurare = new JButton("Numar asigurare");
		btnNumarAsigurare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String nr = JOptionPane.showInputDialog("Dati numarul asigurarii: (Ex: 35)");
						int numar = Integer.valueOf(nr);
						ArrayList<Pacient> ar = null;
						ar = conn.Asigurare(numar);
						ModelTabelPacient mod2 = new ModelTabelPacient(ar);
						table.setModel(mod2);
					}
				 catch(NullPointerException exx) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus date");				
				} catch(NumberFormatException ex) {
					JOptionPane.showMessageDialog(Gestionare_Pacienti.this, "Nu ati introdus date");
				}
			}
		});
		btnNumarAsigurare.setToolTipText("Numar asigurare");
		btnNumarAsigurare.setForeground(Color.WHITE);
		btnNumarAsigurare.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNumarAsigurare.setBackground(new Color(102, 0, 102));
		btnNumarAsigurare.setBounds(386, 429, 143, 33);
		panel_1.add(btnNumarAsigurare);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(new Color(255, 255, 255));
		panel_6.setBounds(65, 323, 59, 2);
		panel_1.add(panel_6);
		
		JPanel panel_6_1 = new JPanel();
		panel_6_1.setBackground(Color.WHITE);
		panel_6_1.setBounds(239, 323, 59, 2);
		panel_1.add(panel_6_1);
		
		JPanel panel_6_2 = new JPanel();
		panel_6_2.setBackground(Color.WHITE);
		panel_6_2.setBounds(426, 323, 59, 2);
		panel_1.add(panel_6_2);
		
		JPanel panel_6_3 = new JPanel();
		panel_6_3.setBackground(Color.WHITE);
		panel_6_3.setBounds(426, 472, 59, 2);
		panel_1.add(panel_6_3);
		
		JPanel panel_6_4 = new JPanel();
		panel_6_4.setBackground(Color.WHITE);
		panel_6_4.setBounds(239, 472, 59, 2);
		panel_1.add(panel_6_4);
		
		JPanel panel_6_5 = new JPanel();
		panel_6_5.setBackground(Color.WHITE);
		panel_6_5.setBounds(65, 472, 59, 2);
		panel_1.add(panel_6_5);
	}
}
