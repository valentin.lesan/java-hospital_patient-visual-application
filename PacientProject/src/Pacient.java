
public class Pacient {
		int IdPacient ;
		String NumePacient ;
		String PrenumePacient ;
		String PatronimicPacient ;
		String Adresa ;
		String Telefon ;
	    int NrPolitaAsigurare;
	    String Sectia ;
	    String Diagnoza ;
	    
		public Pacient(int idPacient, String numePacient, String prenumePacient, String patronimicPacient,
				String adresa, String telefon, int nrPolitaAsigurare, String sectia, String diagnoza) {
			super();
			IdPacient = idPacient;
			NumePacient = numePacient;
			PrenumePacient = prenumePacient;
			PatronimicPacient = patronimicPacient;
			Adresa = adresa;
			Telefon = telefon;
			NrPolitaAsigurare = nrPolitaAsigurare;
			Sectia = sectia;
			Diagnoza = diagnoza;
		}

		@Override
		public String toString() {
			return "Pacient [IdPacient=" + IdPacient + ", NumePacient=" + NumePacient + ", PrenumePacient="
					+ PrenumePacient + ", PatronimicPacient=" + PatronimicPacient + ", Adresa=" + Adresa + ", Telefon="
					+ Telefon + ", NrPolitaAsigurare=" + NrPolitaAsigurare + ", Sectia=" + Sectia + ", Diagnoza="
					+ Diagnoza + "]";
		}
	    
		
		
	    
	    

}
