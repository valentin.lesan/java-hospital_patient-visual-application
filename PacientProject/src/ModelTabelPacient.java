import java.util.*;

import javax.swing.table.AbstractTableModel;
public class ModelTabelPacient extends AbstractTableModel {
	static final int Id_Pacient = 0 ;
	static final int NumePacient = 1;
	static final int PrenumePacient = 2;
	static final int PatronimicPacient = 3;
	static final int Adresa = 4;
	static final int Telefon = 5;
	static final int NrPolitaAsigurare = 6;
	static final int Sectia = 7;
	static final int Diagnoza = 8;
	
	
	
	

	public String[] col = { "Id_Pacient", "Nume Pacient", "Prenume Pacient","PatronimicPacient","Adresa", "Telefon", "Nr Polita Asigurare","Sectia","Diagnoza"};
		
		private ArrayList<Pacient> pacient;
		public ModelTabelPacient(ArrayList<Pacient> pacient) {
			this.pacient= pacient;
		}
		
		@Override
		public int getColumnCount() {
		return col.length;
		}
		
		@Override
		public int getRowCount() {
		return pacient.size();
		}
		
		@Override
		public String getColumnName(int coloane) {
		return col[coloane];
		}


		@Override
		public Object getValueAt(int row, int col) {
			Pacient pacient_temporar = pacient.get(row);
		switch (col) {
		case Id_Pacient :
			return pacient_temporar.IdPacient;
		case NumePacient : 
			return pacient_temporar.NumePacient;
		case	PrenumePacient :
			return pacient_temporar.PrenumePacient;
		case	PatronimicPacient :
			return pacient_temporar.PatronimicPacient;
		case	Adresa :
			return pacient_temporar.Adresa;
		case	Telefon :
			return pacient_temporar.Telefon;
		case	NrPolitaAsigurare :
			return pacient_temporar.NrPolitaAsigurare;
		case	Sectia :
			return pacient_temporar.Sectia;
		case	Diagnoza :
			return pacient_temporar.Diagnoza;
		default:
		return pacient_temporar.IdPacient;
		}}
		
		public Class<? extends Object> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
			}

}
