import java.sql.*;
import java.util.*;

 class ConexiuneBD {
	 public static ArrayList<Pacient> pacient = new ArrayList<Pacient>();
		static Connection conn;
		String databaseURL, user, pass;
		Statement s;
		
		public void conectarebd() {
			databaseURL = "jdbc:mysql://localhost:3306/Pacienti";
			user = "root";
			pass = "";
			conn = null;
			
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection(databaseURL, user, pass);
				if(conn != null) {
					System.out.println("Conexiune cu succes");
				}
			}
			catch(ClassNotFoundException ex)
			{
				System.out.println("Driverul lipseste");
				ex.printStackTrace();
				} 
			catch (SQLException e) {
				System.out.println("Eroare cu baza de date");
				e.printStackTrace();
			}
			
		}
		
		
		public void distrugereconex() {
			if(conn != null) {
				try {
					conn.close();
				}
				catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
//		Afisarea datelor din baza de date
		ArrayList<Pacient> afiseazaPacientii()
		{
			 
			try {
				String sql = "Select * from Pacient";
				Statement stat = conn.createStatement();
				ResultSet result = stat.executeQuery(sql);
				while(result.next()) {
					int IdPacient  = result.getInt("IdPacient");
					String NumePacient = result.getString("NumePacient");
					String PrenumePacient = result.getString("PrenumePacient");
					String PatronimicPacient = result.getString("PatronimicPacient");
					String Adresa = result.getString("Adresa");
					String Telefon = result.getString("Telefon");
				    int NrPolitaAsigurare = result.getInt("NrPolitaAsigurare");
				    String Sectia = result.getString("Sectia");
				    String Diagnoza = result.getString("Diagnoza");
				    
				    pacient.add(new Pacient(IdPacient, NumePacient, PrenumePacient, PatronimicPacient, Adresa, Telefon, NrPolitaAsigurare,Sectia,Diagnoza));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			} return pacient;
		}
		
//		 Afișarea datelor pacientului cu un număr anumit al poliței de asigurare;
		ArrayList<Pacient> Asigurare(int numar)
		{
				ArrayList<Pacient> lista2 = new ArrayList<Pacient>();
				for(Pacient i : pacient) {
					if(i.NrPolitaAsigurare == numar) {
						lista2.add(i);
					}
				}
			
			 return lista2;
		}
		
//		Afișarea listei pacienților cu o diagnoză anumită;
		ArrayList<Pacient> Diagnoza(String diagnoza)
		{
			ArrayList<Pacient> lista2 = new ArrayList<Pacient>();
			for(Pacient i: pacient) {
				if(i.Diagnoza.compareTo(diagnoza) == 0) {
					lista2.add(i);
				}
			}
			return lista2;
		}
		
//		Afișarea datelor pacienților cu numărul poliței de asigurare cuprinse într-un interval anumit;
				ArrayList<Pacient> Polita_Asigurare_Diapazon(String polita_min, String polita_max) {
					ArrayList<Pacient> lista2 = new ArrayList<Pacient>();
						int min = Integer.valueOf(polita_min);
						int max = Integer.valueOf(polita_max);
						for(Pacient i : pacient) {
						
							if(i.NrPolitaAsigurare >= min && i.NrPolitaAsigurare <= max) {
							lista2.add(i);
							}
						}
					return lista2;
					}
		
//				Afișarea numărului de pacienți dintr-o secție anumită;
				ArrayList<Pacient> Pacient_Sectie(String sectia) {
					ArrayList<Pacient> lista2 = new ArrayList<Pacient>();
						for(Pacient i : pacient) {
						
							if(i.Sectia.compareTo(sectia) == 0) {
							lista2.add(i);
							}
						}
					return lista2;
					}
				
//				Afișarea datelor paciențiilor după un prenume anumit.
				ArrayList<Pacient> Pacient_Prenume(String Prenume) {
					ArrayList<Pacient> lista2 = new ArrayList<Pacient>();
					for(Pacient i : pacient) {
							if(i.PrenumePacient.compareTo(Prenume) == 0) {
							lista2.add(i);
							}
						}
					return lista2;
					}
		
}
